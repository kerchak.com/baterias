﻿var i = document.body;
var nivel;
document.addEventListener("DOMContentLoaded", function() {
    bateria();
    });

    function bateria() {
        navigator.getBattery().then(function(battery) {
            battery.addEventListener("levelchange", function() {
                nivel = (battery.level * 100).toFixed(0);
                document.getElementById("pbat").textContent = nivel + "%";
                console.log('Nivel de batería ' + nivel + '%');
                actualizoImagenNivel(nivel);
                if(battery.charging) {document.title = "Cargando...";}
                    else {document.title = "Descargando...";}
            })
        });
    }

    function actualizoImagenNivel(n) {
        console.log("valor de N " + n);
        if (n <= 100 && n >= 30) {
            i.style.backgroundImage = "url(imagenes/green.png)";
            console.info("Batería normal.");
        }
        if (n <= 29 && n >= 20) {
            i.style.backgroundImage = "url(imagenes/orange.png)";
            console.warn("Batería baja.");
        }
        if (n <= 19) {
            i.style.backgroundImage = "url(imagenes/red.png)";
            console.error("Batería muy baja. ¡Conecte el cargador!");
        }
}